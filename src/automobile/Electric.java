package automobile;

import org.w3c.dom.ranges.RangeException;

public class Electric extends Automobile {
	
	int numberOfBatteries;
	boolean tesla;

	

	public Electric(boolean specialLicense, boolean renewable, int basePrice, int vin, int vehicleRange,
			int vehicleCode) throws IllegalAccessException {
		
		if (specialLicense == true && renewable == true) {
			
			if (50 <= vehicleRange && vehicleRange <= 499) {
			
			setBasePrice(basePrice);
				setVin(vin);
			setVehicleRange(vehicleRange);
			setVehicleCode(vehicleCode);
			
			} else {
			
				throw new RangeException((short) 499, "Out of range");
			
			
			}
		}
			}


	@Override
	public void display() {
		
		setManufacturerName("Nissan");
		setColor("White");
	

		System.out.println(VehicleType.ELECTRIC + " by " + getManufacturerName() + " with VIN " + getVin()
				+ " is available to rent in " + getColor() + ".This monster has a range of " + getVehicleRange()
				+ " and only cost unless range exceeded.");

	}

}
