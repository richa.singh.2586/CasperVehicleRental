package automobile;

public class FareCalculator {

	public static void main(String[] args) throws IllegalAccessException, IllegalArgumentException {

		try {
			Electric leaf = new Electric(true, true, 2, 800, 300, 100);
			double fare = leaf.cost();
			System.out.println(fare);
			leaf.display();

		} catch (IllegalAccessException e) {

			e.printStackTrace();
		}

		try {
			Standard golf = new Standard(2, 800, 600, 100);
			double fare = golf.cost();
			System.out.println(fare);
			golf.display();

		} catch (IllegalArgumentException e) {

			e.printStackTrace();
		}

		try {
			SemiTruck tesla = new SemiTruck(true, 2, 800, 300, 500);
			double fare = tesla.cost();
			System.out.println(fare);
			tesla.display();

		} catch (IllegalArgumentException e) {

			e.printStackTrace();
		}

		try {
			Diesel vento = new Diesel(2, 800, 300, 400);
		double fare = vento.cost();
		System.out.println(fare);
		vento.display();
		} catch (IllegalArgumentException e) {

			e.printStackTrace();
		}

	}

}
