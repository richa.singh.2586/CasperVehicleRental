package automobile;

public class SemiTruck extends Diesel {

	private int licenseNumber;

	public int getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(int licenseNumber) {
		this.licenseNumber = licenseNumber;
	}


	public SemiTruck(boolean specialLicense, int basePrice, int vin, int vehicleRange, int vehicleCode) {

		super(basePrice, vin, vehicleRange, vehicleCode);
		if (specialLicense == true && (50 <= vehicleRange && vehicleRange <= 499)) {

			setBasePrice(basePrice);
			setVin(vin);
			setVehicleRange(vehicleRange);
			setVehicleCode(vehicleCode);
		} else

			throw new IndexOutOfBoundsException();
	}

	@Override
	public void display() {

		setManufacturerName("Nissan");
		setColor("White");

		System.out.println(VehicleType.SEMITRUCK + " by " + getManufacturerName() + " with VIN " + getVin()
				+ " is available to rent in " + getColor() + ".This monster has a range of " + getVehicleRange()
				+ " and only cost unless range exceeded.");

	}

}
