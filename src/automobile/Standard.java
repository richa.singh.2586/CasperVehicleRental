package automobile;
import java.util.Date;

public class Standard extends Automobile {

	@SuppressWarnings("deprecation")
	Date emissionTestDate = new Date(2017, 12, 31);
	int numberOfSeats = 5;

	public Date getEmissionTestDate() {
		return emissionTestDate;
	}

	public void setEmissionTestDate(Date emissionTestDate) {
		this.emissionTestDate = emissionTestDate;
	}

	public int getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(int numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	public Standard(int basePrice, int vin, int vehicleRange, int vehicleCode) {

		if (vehicleRange > 0) {

			setBasePrice(basePrice);
			setVin(vin);
			setVehicleRange(vehicleRange);
			setVehicleCode(vehicleCode);

		} else {

			throw new IllegalArgumentException();
		}

	}

	public void checkEmissionDate(Date date) {
		
		if (date.after(date)) {
			
			System.out.println("Passed Emission Date");

		}
	}


	@Override
	public void display() {
		
		setManufacturerName("Nissan");
		setColor("Blue");
	

		System.out.println(VehicleType.STANDARD + " by " + getManufacturerName() + " with VIN " + getVin()
				+ " is available to rent in " + getColor() + ".This beast has a range of " + getVehicleRange()
				+ " and only cost ");

	}
	}


