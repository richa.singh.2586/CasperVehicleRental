package automobile;

public class Diesel extends Automobile {

	int numberOfWheels;
	int amountOfCylinders;

	public Diesel(int basePrice, int vin, int vehicleRange, int vehicleCode) {

		if (vehicleRange > 0) {

			setBasePrice(basePrice);
			setVin(vin);
			setVehicleRange(vehicleRange);
			setVehicleCode(vehicleCode);

		} else {
			throw new IllegalArgumentException();
		}
	}


	@Override
	public void display() {

		setManufacturerName("Nissan");
		setColor("Blue");

		System.out.println(VehicleType.DIESEL + " by " + getManufacturerName() + " with VIN " + getVin()
				+ " is available to rent in " + getColor() + ".This beast has a range of " + getVehicleRange()
				+ " and only cost ");

	}

}
