package automobile;

public abstract class Vehicle {

	protected int vehicleRange;
	protected int vin;
	protected int basePrice;
	protected int vehicleCode;
	protected double luxuryTax = 0;

	public int getVehicleRange() {
		return vehicleRange;
	}

	public void setVehicleRange(int vehicleRange) {
		this.vehicleRange = vehicleRange;
	}

	public int getVin() {
		return vin;
	}

	public void setVin(int vin) {
		this.vin = vin;
	}

	public int getBasePrice() {
		return basePrice;
	}

	public void setBasePrice(int basePrice) {
		this.basePrice = basePrice;
	}

	public int getVehicleCode() {
		return vehicleCode;
	}

	public void setVehicleCode(int vehicleCode) {
		this.vehicleCode = vehicleCode;
	}

	public double cost() {
		double cost = ((basePrice * (vin / vehicleCode)) * vehicleRange);
		return cost;
	};

	public abstract void display();



}
