package automobile;

public abstract class Automobile extends Vehicle {

	private double luxuryTax;
	private String color = "Black";
	private static String manufacturerName;

	public static String getManufacturerName() {
		return manufacturerName;
	}

	public static void setManufacturerName(String manufacturerName) {
		Automobile.manufacturerName = manufacturerName;
	}

	public void display(String manufacturerName, String color, VehicleType S) {


	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
